package com.example.boud.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import android.provider.Settings;

import com.example.boud.myapplication.model.DataMeteo;
import com.squareup.picasso.Picasso;

import java.security.spec.PSSParameterSpec;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ImageView image;
    Button button;
    ImageButton locate;
    Meteo m;
    DataMeteo data;
    private static String APIkey = "9362f2937fbad5726bfdd30bac094450";
    private DataMeteo meteo;
    //TextView t ;
    ListView l;
    EditText Ville;
    TextView TempMax, TempMin, CielT, GPS;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //todo
                    return true;
                case R.id.navigation_accel:
                    switchToAccel();
                    return true;
            }
            return false;
        }
    };

    // GPS
    private LocationManager locationManager;

    private double latitude;
    private double longitude;
    private double altitude;
    private float accuracy;

    Context context;

    LocationListener locationListener;

    /* Necessaire pour permission contrairement a ce qu'indique android studio */
    private int PERMISSION_GPS;
    MeteoService Mservice;
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        retrofit = null;
        //Button Submit
        button = (Button) findViewById(R.id.button);
        // Button GPS
        locate = findViewById(R.id.GPSbutton);
        //Les text wiew
        Ville = findViewById(R.id.editText2Ville);
        TempMax = findViewById(R.id.textViewTempMax);
        TempMin = findViewById(R.id.textViewTempMin);
        CielT = findViewById(R.id.textViewCiel);
        GPS = findViewById(R.id.textViewGPS);
        context = this;

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        /* Methode retrofit Builder */
        connect();
        //methode pour GPS
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                String msg = "New Latitude: " + location.getLatitude()
                        + "New Longitude: " + location.getLongitude();
                //Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                //Ancienne methode removeUpdate
                //locationManager.removeUpdates(this);

                // Si on ne met pas l'appel de getWetherByGPS ici mais dans la button, l'actualalisation du GPS se fait apres et donc les lattidue, longititude sont a
                // Methode pour attendre l'actualisation du GPS ?
                String msg2 = String.valueOf(latitude) + "   " + String.valueOf(longitude);
                GPS.setText(msg2);
                getWetherByGPS();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
                Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                        Toast.LENGTH_SHORT).show();
            }
        };


        //Changement de methode GPS pour l'utilser le positionnement a chaque
        //fois qu'on appuie sur un bouton, tout en economisant la betterie
        // Au lieu de locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        // qui retrouner la dernière valeur et donc pouvait être fausse, si la maj de la position ne fasait pas.

        // Documentation https://developer.android.com/reference/android/location/Criteria.html
        final Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE); // Precision
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Power Consommation maximal
        criteria.setAltitudeRequired(false); // Choisir si alititude necessaire
        criteria.setBearingRequired(false); // Utilisation de palie ? Pas compris
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH); // Si on veut longitude latitude
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

        // location manager
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final Looper looper = null;

        /*
        Buton submit methode metheo via la ville => getWheather(VilleParam,APIkey)
        */
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String varVille;
                //Afficher une image qui illustre la méteo , par exemple icone pluie, ou soleil mais le format des icones trop petit
                //Picasso.with(getApplicationContext()).load("http://openweathermap.org/img/w/{id_image}.png").into(image);
                //Picasso.with(getApplicationContext()).load("http://openweathermap.org/img/w/10d.png").into(image);
                varVille = Ville.getText().toString();
                if (!varVille.isEmpty()) {
                    getWether(varVille);
                }
                System.out.println(data);
            }
        });

        //button GPS methode getmetheo via les coordonnées => getWheatherByGPS(VilleParam,APIkey)
        locate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Verfier Permission GPS
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                    //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                      //      2000,
                        //    10, locationListener);
                    locationManager.requestSingleUpdate(criteria, locationListener, looper);
                    String msgAtt = "Veuillez attendre pendant la sync GPS";
                    Toast.makeText(getBaseContext(), msgAtt, Toast.LENGTH_LONG).show();

                    //Sinon on ouvre un popup qui demande
                }else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSION_GPS);

                }

            }
        });
    }


    public void switchToAccel(){
        Intent intent = new Intent(this, AccelActivity.class);
        startActivity(intent);
    }

        /* Methode appel interface MeteoService => methode => getWheatherbyGPS */

    public void getWetherByGPS(){
        Call<DataMeteo> dataGPS = Mservice.getWheatherByGPS(this.latitude,this.longitude,APIkey);
        dataGPS.enqueue(new Callback<DataMeteo>() {
            @Override
            public void onResponse(Call<DataMeteo> call, Response<DataMeteo> response) {
                if (response.body() != null) {
                    meteo = response.body();
                    afficher();
                    String VilleAffichage = meteo.getVille();
                    Ville.setText(VilleAffichage);
                }
            }

            @Override
            public void onFailure(Call<DataMeteo> call, Throwable t) {
            }
        });
    }

    /* Methode appel interface MeteoService => methode => getWheather */
    public void getWether(String VilleParam){

        Call<DataMeteo> c = Mservice.getWheather(VilleParam,APIkey);

        c.enqueue(new Callback<DataMeteo>() {
            @Override
            public void onResponse(Call<DataMeteo> call, Response<DataMeteo> response) {
                if (response.body() != null) {
                    meteo = response.body();
                    afficher();
                }
            }

            @Override
            public void onFailure(Call<DataMeteo> call, Throwable t) {
            }
        });
    }

    // Retrofit build sur l'api openWhetherMap
    public void connect (){
        if (retrofit ==null) {
                    retrofit = new Retrofit.Builder()
                    .baseUrl("http://api.openweathermap.org/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Mservice = retrofit.create(MeteoService.class);
        }
    }

    public void afficher(){
        double temp_min = meteo.getMainData().getTemp_min();
        double temp_max = meteo.getMainData().getTemp_max();
        temp_max = temp_max - 273.15;
        temp_min = temp_min - 273.15;
        String Tmax = String.valueOf(temp_max) + " ° C";
        String Tmin = String.valueOf(temp_min) + " ° C";
        TempMax.setText(Tmax);
        TempMin.setText(Tmin);
        String ciel = meteo.getWeather().get(0).getMain();
        String cieldata = meteo.getWeather().get(0).getDescription();
        CielT.setText(ciel + ", " + cieldata);
    }

}
