package com.example.boud.myapplication;

import com.example.boud.myapplication.model.DataMeteo;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import com.example.boud.myapplication.model.mainData;

/**
 * Created by boud on   16/12/17  .
 */


public interface MeteoService {

@GET("data/2.5/weather")
    Call<DataMeteo> getWheather(@Query("q") String Ville, @Query("appid") String ApiKey);

@GET("data/2.5/weather")
    Call<DataMeteo> getWheatherByGPS(@Query("lat") Double latitude,@Query("lon") Double longititude, @Query("appid") String ApiKey);



}




//http://api.openweathermap.org/data/2.5/weather?q=Nancy&appid=9362f2937fbad5726bfdd30bac094450