package com.example.boud.myapplication.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by boud on 16/12/17.
 */

public class DataHeure {
    @SerializedName("sunrise")
    private double sunrise;
    @SerializedName("sunset")
    private double sunset;

    public DataHeure(double sunrise, double sunset) {
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    @Override
    public String toString() {
        return "DataHeure{" +
                "sunrise=" + sunrise +
                ", sunset=" + sunset +
                '}';
    }

    public double getSunrise() {
        return sunrise;
    }

    public void setSunrise(double sunrise) {
        this.sunrise = sunrise;
    }

    public double getSunset() {
        return sunset;
    }

    public void setSunset(double sunset) {
        this.sunset = sunset;
    }
}
