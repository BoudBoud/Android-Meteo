package com.example.boud.myapplication.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by boud on 16/12/17.
 */

public class Coord {
    @SerializedName("lon")
    private double lon;

    @SerializedName("lat")
    private double lat;


    public Coord(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "Coord{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
