package com.example.boud.myapplication;

import com.example.boud.myapplication.model.DataMeteo;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by boud on 16/12/17.
 *
 * Ne sert plus a rien, test pour separer le code matier du la classe MainActivity
 * Tentative d'organisation du code en plusieur classe pour plus propore mais pas implémenter apres plusieurs erreur
 *
 */

public class Meteo {

    private static String APIkey = "9362f2937fbad5726bfdd30bac094450";
    private DataMeteo meteo;

    public void connect (){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MeteoService Mservice = retrofit.create(MeteoService.class);

        Call<DataMeteo> c = Mservice.getWheather("Nancy",APIkey);

        c.enqueue(new Callback<DataMeteo>() {
            @Override
            public void onResponse(Call<DataMeteo> call, Response<DataMeteo> response) {
                meteo = response.body();



            }

            @Override
            public void onFailure(Call<DataMeteo> call, Throwable t) {

            }
        });
    }

    public DataMeteo getMeteo() {
        return meteo;
    }
}

//http://api.openweathermap.org/data/2.5/weather?q=Nancy&appid=9362f2937fbad5726bfdd30bac094450