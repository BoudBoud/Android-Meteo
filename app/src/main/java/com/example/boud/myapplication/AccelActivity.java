package com.example.boud.myapplication;


import android.content.Context;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.MenuItem;
import android.widget.TextView;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.ImageView;



public class AccelActivity extends AppCompatActivity implements SensorEventListener{
    //declaration variables
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private TextView accX, accY, accZ;

    //Pour creer le dessin
    private ImageView drawingImageView;
    private Canvas canvas;
    private int centerX;
    private int centerY;

    private float[] linear_acceleration = new float[3];

    //Gestion de la barre de navigation inferieure
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    backToMain();
                    return true;
                case R.id.navigation_accel:

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accel);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //declaration des objets a afficher
        accX = findViewById(R.id.accX);
        accY = findViewById(R.id.accY);
        accZ = findViewById(R.id.accZ);
        drawingImageView = (ImageView) this.findViewById(R.id.DrawingImageView);

        //Pour obtenir hauteur/largeur du dessin
        // /!\ API obsololete
        Display d = getWindowManager().getDefaultDisplay();
        centerX = d.getWidth() / 2;
        centerY = d.getHeight() / 2;

        //creation de la bitmap (matrice de pixels) et insertion dans le canvas
        Bitmap bitmap = Bitmap.createBitmap(d.getWidth(),d.getHeight(), Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        drawingImageView.setImageBitmap(bitmap);

        //Declaration de la gestion des capteurs
        //Choix du capteur (accelerometre) et du type de données (linéaire > pas de gravité inclue dans les valeurs)
        // (pas linéraire) > gravité présente : utile pour mesurer une orientation, en plus d'une force !
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
    }

    public void drawForce(int x, int y){
        // On dessine un ligne a partir du centre, dont la direction depend des valeurs de l accelerometre
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        Paint paint = new Paint();
        paint.setColor(Color.rgb(255, 153, 51));
        paint.setStrokeWidth(10);
        int startx = centerX;
        int starty = centerY;
        int endx = x + centerX;
        int endy = y + centerY;
        canvas.drawLine(startx, starty, endx, endy, paint);

    }

    //Pour quitter l'activité
    public void backToMain(){
        finish();
    }

    //mesures en pause/reprise si on quitte/reprend l'activité (economie énergie)

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    //A chaque nouvelle valeur, on met a jour l'affichage
    public void onSensorChanged(SensorEvent event){
        //if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
        //if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            linear_acceleration[0] = event.values[0];
            linear_acceleration[1] = event.values[1];
            linear_acceleration[2] = event.values[2];

            accX.setText("X: " + String.valueOf(linear_acceleration[0]) );
            accY.setText("Y: " + String.valueOf(linear_acceleration[1]) );
            accZ.setText("Z: " + String.valueOf(linear_acceleration[2]) );

            //Facteur de 100pixels pour avoir un graphique assez visible
            int x = (int)(linear_acceleration[0]*100);
            int y = -(int)(linear_acceleration[1]*100);

            drawForce(x,y);
    }

}
