package com.example.boud.myapplication.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by boud on 16/12/17.
 */

public class DataMeteo {
    @SerializedName("coord")
    private Coord coord;
    @SerializedName("weather")
    private List<Weather> weather = null;
    @SerializedName("base")
    private String base;
    @SerializedName("main")
    private mainData mainData;
    @SerializedName("wind")
    private Wind wind;
    @SerializedName("sys")
    private DataHeure dataHeure;
    @SerializedName("name")
    private String Ville;
    @SerializedName("visibility")
    private String visibility;

    public DataMeteo(Coord coord, List<Weather> weather, String base, mainData mainData, Wind wind, DataHeure dataHeure, String ville, String visibility) {
        this.coord = coord;
        this.weather = weather;
        this.base = base;
        this.mainData = mainData;
        this.wind = wind;
        this.dataHeure = dataHeure;
        this.Ville = ville;
        this.visibility = visibility;
    }

    @Override
    public String toString() {
        return "DataMeteo{" +
                "coord=" + coord +
                ", weather=" + weather +
                ", base='" + base + '\'' +
                ", mainData=" + mainData +
                ", wind=" + wind +
                ", dataHeure=" + dataHeure +
                ", Ville='" + Ville + '\'' +
                ", visibility='" + visibility + '\'' +
                '}';
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }


    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }



    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public com.example.boud.myapplication.model.mainData getMainData() {
        return mainData;
    }

    public void setMainData(mainData mainData) {
        this.mainData = mainData;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public DataHeure getDataHeure() {
        return dataHeure;
    }

    public void setDataHeure(DataHeure dataHeure) {
        this.dataHeure = dataHeure;
    }

    public String getVille() {
        return Ville;
    }

    public void setVille(String ville) {
        Ville = ville;
    }
}
