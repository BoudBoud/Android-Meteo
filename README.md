# Projet Mobile Android

Le fichier APK est disponible dans l'onglet 
[Tags](https://gitlab.com/BoudBoud/Android-Meteo/tags/v1.0)


Cette application Android contient 2 activités :

* Météo avec requête sur l'api d'OpenWeatherMap à l'aide de Retrofit
* Accéléromètre : affichage des valeurs et de l'orientation avec un graphique

### Utilisation hardware :
* GPS
* Accéléromètre

#### Par Mehdi Boudart (@BoudBoud) & Alban Boyé (@Alban_B)
